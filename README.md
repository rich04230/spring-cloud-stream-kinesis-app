# spring-cloud-stream-kinesis-app

Implement event-driven microservices using AWS Kinesis and Spring Cloud

## Reference
- [Spring Cloud Stream Kinesis Binder](https://github.com/spring-cloud/spring-cloud-stream-binder-aws-kinesis/blob/master/spring-cloud-stream-binder-kinesis-docs/src/main/asciidoc/overview.adoc)
- [Spirng Integration AWS](https://github.com/spring-projects/spring-integration-aws)
- [AWS SDK](https://docs.aws.amazon.com/streams/latest/dev/developing-consumers-with-kcl.html)
- [Spring Cloud Stream](https://docs.spring.io/spring-cloud-stream/docs/Elmhurst.RELEASE/reference/htmlsingle/#consumer-groups)