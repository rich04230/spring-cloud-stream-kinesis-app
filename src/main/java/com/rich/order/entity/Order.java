package com.rich.order.entity;

import lombok.Data;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Entity
@Table(name = "ORDER_TB")
public class Order {

	@Id
	@Column(name = "ID")
	private UUID id;

	@Column(name = "NAME")
	private String name;

	public Order() {
		id = UUID.randomUUID();
	}
}
