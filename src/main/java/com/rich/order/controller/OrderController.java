package com.rich.order.controller;

import com.rich.order.entity.Order;
import com.rich.order.repository.OrderRepository;
import com.rich.order.stream.Event;
import com.rich.order.stream.OrderProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class OrderController {

	@Autowired
	private OrderRepository orders;

	@Autowired
	private OrderProducer orderProducer;

	@Value("${spring.application.name}")
	private String appName;

	@RequestMapping(value = "/orders", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(HttpStatus.OK)
	public Iterable<Order> getOrder() {
		Iterable<Order> orderList = orders.findAll();

		return orderList;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Order> add(@RequestBody Order input) {
		orders.save(input);

		// place order on Kinesis Stream
		orderProducer.sendOrder(new Event(input, "ORDER", appName));

		return new ResponseEntity<Order>(input, HttpStatus.OK);
	}

}
