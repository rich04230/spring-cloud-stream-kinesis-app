package com.rich.order.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface OrderProcessor {

	String INPUT = "ordersIn";
	String OUTPUT = "ordersOut";

	@Output(OUTPUT)
	MessageChannel ordersOut();

	@Input(INPUT)
	SubscribableChannel ordersIn();

}
