package com.rich.order.stream;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@Slf4j
@EnableBinding(OrderProcessor.class)
public class OrderConsumer {

    @StreamListener(OrderProcessor.INPUT)
    public void consume(Event event) {
        log.info("An order event has been received: {}", event);
    }
}
