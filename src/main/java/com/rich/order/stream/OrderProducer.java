package com.rich.order.stream;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderProducer {

    @Autowired
    private OrderProcessor orderOut;

    public void sendOrder(Event event) {
        orderOut.ordersOut().send(new GenericMessage<>(event));
        log.info("Event sent: {}", event);
    }

}
