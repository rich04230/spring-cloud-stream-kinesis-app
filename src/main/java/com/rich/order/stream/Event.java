package com.rich.order.stream;

import java.time.Instant;
import java.util.UUID;

import com.rich.order.entity.Order;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rich
 */
@Data
@NoArgsConstructor
public class Event {

	private String id;

	private Order subject;

	private String type;

	private String originator;

	private long createAt;

	public Event(Order subject, String type, String originator) {
		this.subject = subject;
		this.type = type;
		this.originator = originator;
		this.id = UUID.randomUUID().toString();
		this.createAt = Instant.now().toEpochMilli();
	}

}
